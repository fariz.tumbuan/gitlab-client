import logging
import os
import requests

GIT_API = 'https://gitlab.com/api/v4'


def check_token(git_function):
    def wrapper(self, access_token=None, private_token=None, *args, **kwargs):
        if not (access_token or private_token):
            raise ValueError('No git tokens found.')
        return git_function(
            self, access_token=access_token, private_token=private_token, *args,
            **kwargs
        )
    return wrapper


def build_request(access_token, private_token, *args, **kwargs):
    # name
    # namespace_id
    # description
    # lfs enabled
    headers = {}
    if access_token:
        headers['Authorization'] = f'Bearer {access_token}'
    else:
        headers['Private-Token'] = private_token

    params = {}
    for key, val in kwargs.items():
        params[key] = val
    return headers, params


class GitClient():
    def __init__(self, api=GIT_API):
        self.api = api

    def get_user(
            self,
            username=None,
            *args,
            **kwargs
        ):
        api_uri = (
            os.path.join(self.api, 'users')
            + (f'?username={username}' if username else '')
        )
        resp = requests.get(api_uri)
        if resp.status_code != 200:
            resp.raise_for_status()
        return resp.json()

    @check_token
    def list_projects(
            self,
            access_token=None,
            private_token=None,
            user_id=None,
            *args,
            **kwargs
        ):
        assert user_id, 'Missing user id'
        headers, params = build_request(
            access_token, private_token, *args, **kwargs
        )
        resp = requests.get(
            os.path.join(self.api, 'users', str(user_id), 'projects'),
            headers=headers,
        )
        if resp.status_code != 200:
            resp.raise_for_status()
        return resp.json()


    @check_token
    def get_project(
            self,
            access_token=None,
            private_token=None,
            project_id=None,
            *args,
            **kwargs
        ):
        assert project_id, 'Missing project id'
        headers, params = build_request(
            access_token, private_token, *args, **kwargs
        )
        resp = requests.get(
            os.path.join(self.api, 'projects', str(project_id)),
            headers=headers,
        )
        if resp.status_code != 200:
            resp.raise_for_status()
        return resp.json()


    @check_token
    def create_project(
            self,
            access_token=None,
            private_token=None,
            *args,
            **kwargs
        ):
        headers, params = build_request(
            access_token, private_token, *args, **kwargs
        )
        resp = requests.post(
            os.path.join(self.api, 'projects'),
            headers=headers,
            params=params,
        )
        if resp.status_code != 200:
            resp.raise_for_status()
        return resp.json()


    @check_token
    def delete_project(
            self,
            access_token=None,
            private_token=None,
            project_id=None,
            *args,
            **kwargs
        ):
        assert project_id, 'Missing project id'
        headers, params = build_request(
            access_token, private_token, *args, **kwargs
        )
        resp = requests.delete(
            os.path.join(self.api, 'projects', str(project_id)),
            headers=headers,
            params=params,
        )
        if resp.status_code  != 200:
            resp.raise_for_status()
        return resp.json()
